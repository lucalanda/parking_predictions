# Traccia e-learning

dataset https://www.kaggle.com/doit-intl/autotel-shared-car-locations , è un campionamento delle posizioni delle auto di un servizio di car sharing per un anno.
Predire dopo quanto verrebbe riutilizzata un'auto parcheggiata in una certa zona, o dove conviene suggerire agli utenti di lasciare un'auto in base alla zona di arrivo.

Predire dopo quanto verrebbe riutilizzata un'auto parcheggiata in una certa zona in funzione di vari parametri:
- tipologia della zona (periferia, centro, ..)
- stagione (calda, fredda, ..)
- etc

# Predizione del tempo prima del riutilizzo di un'auto lasciata in una certa zona
* tirare fuori un dataset con: <auto, posizione, ora giacenza, tempo di giacenza>

# correzione estrazione parking_duration_df
## NOTA: con il procedimento attuale, se un'auto sta in giro un'ora prima di essere parcheggiata nuovamente, quell'ora conta come tempo di permanenza del suo ultimo parcheggio
## possibile procedimento:
* groupby posizione (lat, lon)
* per ogni gruppo si ordina per timestamp
* currentCars = {}
* per ogni record
  * per ogni carId non più presente in currentCars.keys()
    * creo record: timestamp, lat, lon, <currentTimestamp - currentCars[carId]>
  * per ogni nuovo carId
    * currentCars[carId] = timestamp
### e le currentCars rimanenti una volta finito di ciclare il gruppo?

##### Idee
* stimare quanto spesso un'auto esce dal proprio quartiere?
* stimare quanta distanza percorrerà in base a dov'è stata presa e a che ora?
* suggerimenti su dove lasciare un'auto, in base alla zona in cui ci si trova e altri fattori?
* predire quante auto saranno disponibili per quartiere?
  * incrociarle a informazioni su aree commerciali, asili, hotel, etc?
    * datasets: https://opendata.tel-aviv.gov.il/en/pages/home.aspx#/

# Osservazioni
* Il prof si aspetta l'uso di tecniche viste a lezione per il pre-processing dei dati

