# TODO
* cercare calendario feste israeliano => DONE
  * https://www.officeholidays.com/countries/israel/2018
* creare versione basilare che usa regressione e classificazione (vedi "Step versione base") => DONE
* refactoring nomi variabili => DONE
* creare dataset festività da https://www.officeholidays.com/countries/israel/2018 => DONE
* cercare dataset base completo e significato colonne => DONE
  * verificare se quello completo al 100% è sparito del tutto e basta => DONE
  * test e adattamenti a nuovo dataset completo => DONE
* integrare dataset holiday e attributo isHoliday => DONE
* integrare dati: isWeekend (venerdì e sabato in israele) => DONE
* correzione estrazione parking_duration_df (vedi appunti) => DONE
  * rimuovere vecchio csv cachato e ricrearlo => DONE
  * refactor: rename added_as_endless in qualcosa di decente => DONE
* escludere righe "inaccurate" => DONE
* testare estraendo il dataset da sample_table.csv => DONE
  * => risultati sempre scarsi
* cercare dataset meteo => DONE
  * link: http://www.iacdc.tau.ac.il/data-access/datasets/ims/ims-data/
  * lettura variabili: https://www.dkrz.de/up/services/data-management/projects-and-cooperations/ipcc-data/cmip5-variables
  * NOTA: arrivano fino al 30 giugno 2019, e costringerebbero a escludere il 40% del dataset
* tentare di aggiungere dati pioggia (is_raining si/no) => DONE (non ci sono i dati)
  * link "rain stat" fornisce dati per un solo giorno al mese
  * link "rain" sembra fornire dati solo fino al 2019-04-21 (testare con il vecchio dataset) => incompleti
  * link "hourly" => mancano dati sulle precipitazioni
  * link "daily" => mancano dati sulle precipitazioni

* integrare dati meteo: temperatura
  * spostare più avanti filtraggio record antecedenti al 30 giugno 2019

* valutare se inserire numero di hotel e asili
  * => le prestazioni peggiorano un filo
* valutare se inserire fasce di età
  * testare utilizzo "singolo" e con numero asili e hotel
  * => in tutte le combinazioni i risultati diventano solo un filo peggiori

* TODO prima dell'invio del progetto base
  * rimuovere dataset non necessari
  * controllare TODO
  * inserire almeno un altro classificatore
    * provare MLPClassifier(alpha=1, max_iter=1000) e AdaBoostClassifier()
* prove da fare dopo l'invio del progetto base
  * fare sin e cos di neighborhood_id
  * aggiungere classificatori
  * sostituire neighborhood_id con tutte le sue features?
  * tentare di predire il numero di minuti sotto l'ora, o il numero di ore sopra le 10/12

* correggere import: sample_table o sample_view?
* tuning regressori (vedi sotto)
* tuning classificatori
  * usare grid search cross validation
* idea: usare classificatore per la fascia oraria e regressore per il numero di ore qualora la fascia oraria sia davvero alta (> 6/8 ore)
* verifiche e considerazioni sui null presenti?
* verificare TODO rimasti nel progetto
* rimuovere import non necessari
* valutare dove inserire grafici o considerazioni sui dati
  * es. si può mostrare al caricamento iniziale del dataset qual'è il range temporale considerato (dicembre 2018 a settembre 2019)
* creare file di requirements

# Da valutare
* lettura https://www.kaggle.com/junkal/selecting-the-best-regression-model, idee:
  * valutare se ci sono parametri da rendere logaritmici? (forse il numero di minuti)
  * testare tutti i regressori indicati
  * valutare i regressori con mean_squared_error
* testare i regressori con PolynomialFeatures e GaussianFeatures, se possibile

# Versione migliorata
* si inseriscono più dati (holiday, isWeekend, meteo)
* mostrare correlazioni tra i dati e la permanenza
* si testano tutti i regressori
* classificatore addestrato con params grid
* valutare bilanciamento dataset con le fasce orarie e valutare come riempirle

-------------------------------------------------------------------

# Step versione base
* spiegazione breve dataset
* mapping delle coordinate ai quartieri
* estrazione nuovo dataframe per calcolo della permanenza delle auto
* one hot encoding per i quartieri e normalizzazione dei timestamp
* addestramento e test regressor
* normalizzazione dati per classificatore (es. trasformazione minuti in lassi di tempo predefiniti)
* addestramento e test classificatore

# codice da tenere
#seconds_in_day = 24 * 60 * 60
#seconds_in_week = 7 * seconds_in_day
#parking_duration_df['time_in_seconds'] = pd.to_datetime(time_of_stay_df['timestamp']).values.astype(np.int64)
#sin_time_day = np.sin(2*np.pi*time_of_stay_df['time_in_seconds']/seconds_in_day)
#cos_time_day = np.cos(2*np.pi*time_of_stay_df['time_in_seconds']/seconds_in_day)
#sin_time_week = np.sin(2*np.pi*time_of_stay_df['time_in_seconds']/seconds_in_week)
#cos_time_week = np.cos(2*np.pi*time_of_stay_df['time_in_seconds']/seconds_in_week)


# Regressori da testare
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression, Lasso, ElasticNet, Ridge
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import GradientBoostingRegressor

